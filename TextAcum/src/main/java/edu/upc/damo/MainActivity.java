package edu.upc.damo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    // Nodes del layout i del menú
    private EditText campAfegir;
    private EditText campSuprimir;
    private TextView res;
    private MenuItem menuItemAfegir;
    private MenuItem menuItemSuprimir;
    private ScrollView scroll;

    private Teclat teclat;

    OnCanviModelListener listener;

    public interface OnCanviModelListener{
        public void onCanviModel();
    }

    public void setOnCanviModelListener(OnCanviModelListener listener){
        this.listener = listener;
    }

    public void avisaObservador(){
        listener.onCanviModel();
    }


     private ModelObservable model;

// Mètodes principals del framework

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        inicialitzaMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_ajut:
                mostraAjut();
                return true;
            case R.id.action_esborra:
                esborraResultat();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Inicialitzacions

     private void inicialitza() {
         teclat = new Teclat();

         Vista vista = new Vista(this);
         Model m = new Model();
         model = new ModelObservable(m);
         vista.defineixVisualitzable(new ModelVisualitzable(m));
         model.setOnCanviModelListener(new ModelObservable.OnCanviModelListener() {
             @Override
             public void onNovesDades() {
                 avisaObservador();
             }
         });

     }


    private void inicialitzaMenu(final Menu menu) {
         menuItemAfegir = menu.findItem(R.id.action_afegeix);
         menuItemSuprimir = menu.findItem(R.id.action_esborra_lin);

        scroll = (ScrollView) findViewById(R.id.scroll);


        programaFocus(menuItemAfegir);
        programaAccions(menuItemAfegir);

        programaFocusEsborra(menuItemSuprimir);
        programaAccionsEsborra(menuItemSuprimir);
    }

    //Funcions auxiliars per a la programació dels menús

    private void programaAccions(final MenuItem menuItem) {
        campAfegir =  (EditText) menuItem
                .getActionView()
                .findViewById(R.id.entradaDada);
        campAfegir.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return nouContingut(v, actionId, event);
            }
        });
    }

    private void programaFocus(MenuItem menuItem) {
        // Les dues retro-crides han de retornar true perquè volem tractar

      menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.expandActionView();
                return true;
            }
        });

        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                menuItemSuprimir.collapseActionView();
                campAfegir.requestFocus();
                teclat.mostraTeclat(campAfegir);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                teclat.amagaTeclat(campAfegir);
                return true;
            }
        });
    }



    private void programaAccionsEsborra(MenuItem menuItem) {
        campSuprimir =  (EditText) menuItem
                .getActionView()
                .findViewById(R.id.suprimirDada);
        campSuprimir.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return esborraLinia(v, actionId, event);
            }
        });
    }

    private void programaFocusEsborra(MenuItem menuItem) {
        // Les dues retro-crides han de retornar true perquè volem tractar

        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.expandActionView();
                return true;
            }
        });

        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                menuItemAfegir.collapseActionView();
                campSuprimir.requestFocus();
                teclat.mostraTeclat(campSuprimir);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                teclat.amagaTeclat(campSuprimir);
                return true;
            }
        });
    }

    // Funcions cridades per les opcions de menú


    private void mostraAjut(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setTitle(R.string.app_name)
                .setMessage(getString(R.string.TextAjut))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                    }
                });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void esborraResultat() {
        suprimeixResAnterior();
    }

    private boolean esborraLinia(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)

        switch (teclat.accio(actionId, event)) {
            case Teclat.OK:
                treuDeResultat(campSuprimir.getText());
                campSuprimir.setText("");
                return true;
        }

        return true;
    }


    private boolean nouContingut(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)

        switch (teclat.accio(actionId, event)) {
            case Teclat.OK:
                nouContingut(v.getText().toString());

                v.setText("");  // Esborrem campAfegir

                if (model== new ModelObservable())
                    menuItemSuprimir.setVisible(false);
                else
                    menuItemSuprimir.setVisible(true);

                Toast.makeText(this, getString(R.string.afegirToast), Toast.LENGTH_SHORT).show();
                return true;
        }

        return true;
    }



    private void treuDeResultat(Editable text) {
        int posicio;
        try {
            posicio = Integer.parseInt(text.toString())-1;
        } catch (NumberFormatException e) {
            return;
        }
       try{
            treuDelModel(posicio);
        }
       catch (Exception e){}
    }

    private void treuDelModel(int posicio) {
        model.remove(posicio);
    }


    private void nouContingut(CharSequence text) {
        model.afegir(text);
    }


    private void suprimeixResAnterior() {
        model.buida();
     }


    // --------------------------------  Gestió de teclat

    private class Teclat {
        final static int DESCONEGUT = 0;
        final static int OK = 1;

        private TeclatAbstracte teclat;


        Teclat(){
            if (hiHaTeclatHard())
                teclat= new TeclatHard();
            else
                teclat = new TeclatSoft();
        }

        private void mostraTeclat(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.showSoftInput(v, InputMethodManager.RESULT_SHOWN);
        }

        private void amagaTeclat(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }



        private boolean hiHaTeclatHard() {
            return getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
        }

        public int accio(int actionId, KeyEvent event){
            return teclat.accio(actionId,event);
        }

        private abstract class TeclatAbstracte {

            abstract int accio(int actionId, KeyEvent event);

        }

        private class TeclatHard extends TeclatAbstracte {
            @Override
            int accio(int actionId, KeyEvent event) {
                switch (event.getAction()) {
                    case KeyEvent.ACTION_UP:
                        return OK;
                }
                return DESCONEGUT;
            }
        }

        private class TeclatSoft extends TeclatAbstracte {
            @Override
            int accio(int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_GO:
                        return OK;
                }
                return DESCONEGUT;
            }
        }

    }
}






